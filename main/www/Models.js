var isNode = (typeof document === 'undefined');
var DB_EXISTS = false;
var CONN = null;
var PURGE = true;
var socket = null;
var PURGE = false;

var GET = 0;
var PUT = 1;
var DELETE = 2;

if(isNode) {
    var oopi = require('./libs/oopi/oopi.min');
    var http = require('http');
    var mysql = require('mysql');
    var CONFIG = require('../config.js');
    var Reflection = require('./Reflection');
    Reflection.configure(CONFIG);
} else {
    __dirname = "";
}

function configure(options) {
    Reflection.configure(options);
}

var Cart = Reflection.prototype.extend({
    dbparams: {
    },
    foriegn: ['items'],
    className: 'Cart',
    items:[],
    construct: function () {

    },
    join: function (req,obj,hurdle) {
        hurdle.set();
        Item.prototype.list(null,req,function (list) {
            obj.items = list;
            console.log(list)
            hurdle.complete(obj);
        });
    }
});

var Item = Reflection.prototype.extend({
    dbparams: {
        name:'varchar(100)',
        qty:'int',
        cart: 'varchar(101)'
    },
    className: 'Item',
    name:null,
    qty:0,
    construct: function () {

    }
});

if(isNode) {
    //Reflection.query('drop database '+CONFIG.DB_NAME);
    exports.Item = Item;
    exports.configure = configure;
}