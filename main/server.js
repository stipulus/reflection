var express = require('express');
var models = require('./www/Models');

var data,callback,params,CONN;

var app = express();
var http = require('http');
var server = http.Server(app);

app.use('/',express.static(__dirname + '/www'));

server.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Reflection listening at http://%s:%s', host, port);

});

models.configure({app:app});

function init(req,res,cb) {
    data = {};
    var start = req.url.indexOf('?');
    if(start >= 0) {
        //console.log(decodeURIComponent(req.url.substr(start+1)))
        params = JSON.parse(decodeURIComponent(req.url.substr(start+1)));
    } else {
        params = {};
    }
    if(cb) {
        cb();
    }
}

function output(res) {
    res.send(JSON.stringify(data));
}







